package de.feu.propra18.q8909806;

import javafx.geometry.Point2D;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class ViewLogicTest {

    @Test
    public void getLowestPoint() {
        List<Point> points = new ArrayList<>();
        points.add(new Point(4, 3));
        points.add(new Point(5, 3));
        points.add(new Point(2, 4));
        points.add(new Point(1, 6));
        HullCalculator hullCalculator = new HullCalculator();
        assertEquals(new Point(4, 3), hullCalculator.getLowestYPoint(points));
    }

    @Test
    public void sortByPerspectiveToPointTest() {
        Point reference = new Point(1, 1);

        List<Point> points = new ArrayList<>();
        points.add(reference);
        points.add(new Point(2, 1));
        points.add(new Point(1, 4));
        points.add(new Point(2, 3));
        points.add(new Point(4, 2));
        points.add(new Point(4, 4));
        points.add(new Point(3, 4));
        points.add(new Point(3, 2));
        points.add(new Point(5, 2));
        Collections.shuffle(points);
        List<Point> sorted = new ArrayList<>(points);
        HullCalculator hullCalculator = new HullCalculator();
        hullCalculator.sortRightToleftFromPerspective(reference, sorted);
        List<Point> expected = new ArrayList<>(Arrays.asList(
                new Point(1, 1), new Point(2, 1), new Point(5, 2),
                new Point(4, 2), new Point(3, 2), new Point(4, 4),
                new Point(3, 4), new Point(2, 3), new Point(1, 4)
        ));
        assertEquals(expected, sorted);
    }
}