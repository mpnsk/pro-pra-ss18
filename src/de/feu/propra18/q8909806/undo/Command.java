package de.feu.propra18.q8909806.undo;

public interface Command {

    void execute();
    void undo();
    void redo();
}
