package de.feu.propra18.q8909806.undo;

import de.feu.propra18.q8909806.Point;
import de.feu.propra18.q8909806.view.render.RenderedPoint;

import java.math.BigInteger;

public class MovePoint implements Command {

    private final RenderedPoint renderedPoint;
    private final BigInteger[] oldXY;
    private final BigInteger[] newXY;

    public MovePoint(RenderedPoint point, BigInteger[] oldXY, BigInteger[] newXY) {
        this.renderedPoint = point;
        this.oldXY = oldXY;
        this.newXY = newXY;
    }

    @Override
    public void execute() {
        Point point = renderedPoint.getPoint();
        point.setX(newXY[0]);
        point.setY(newXY[1]);
        renderedPoint.setCircleToPoint();
        System.out.println("moving point to " + point.getX() + '/' + point.getY());
    }

    @Override
    public void undo() {
        Point point = renderedPoint.getPoint();
        point.setX(oldXY[0]);
        point.setY(oldXY[1]);
        renderedPoint.setCircleToPoint();
    }

    @Override
    public void redo() {
        execute();
    }
}
