package de.feu.propra18.q8909806.undo;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.Button;

import java.util.Stack;

public class UndoManager {
    private final Button undoButton;
    private final Button redoButton;
    private Runnable update;
    private Stack<Command> undoStack;
    private Stack<Command> redoStack;

    public UndoManager(Runnable update, Button undoButton, Button redoButton) {
        this.update = update;
        this.undoButton = undoButton;
        this.redoButton = redoButton;
        undoStack = new Stack<>();
        redoStack = new Stack<>();
        updateButtons();
    }


    public void execute(Command command) {
        command.execute();
        undoStack.push(command);
        System.out.println("command = " + command);
        System.out.println("pushed onto undostack");
        redoStack.clear();
        updateButtons();
        update.run();
    }

    public void undo() {
        Command command = undoStack.pop();
        command.undo();
        redoStack.push(command);
        updateButtons();
        update.run();
    }

    public void redo() {
        Command command = redoStack.pop();
        command.redo();
        undoStack.push(command);
        updateButtons();
        update.run();
    }

    private void updateButtons(){
        undoButton.setDisable(undoStack.isEmpty());
        redoButton.setDisable(redoStack.isEmpty());
    }


}
