package de.feu.propra18.q8909806;

import javafx.geometry.Point2D;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.*;

public class GeoUtilTest {


    @Test
    public void leftRight_onTheLine_Test() {
        int[] a = {1, 1};
        int[] b = {3, 3};
        int[] x = {2, 2};
        int xDiff = x[0] - a[0];
        int yDiff = x[1] - a[1];
        System.out.println(Math.toDegrees(Math.atan2(yDiff, xDiff)));
        int lr = GeoUtil.links(a, b, x);
        assertEquals(0, lr);
    }

    @Test
    public void leftRight_left_Test() {
        int[] a = {1, 1};
        int[] b = {3, 3};
        int[] x = {2, 3};
        int lr = GeoUtil.links(a, b, x);
        int xDiff = x[0] - a[0];
        int yDiff = x[1] - a[1];
        System.out.println(Math.toDegrees(Math.atan2(yDiff, xDiff)));
        assertTrue(lr > 0);
    }

    @Test
    public void leftRight_right_Test() {
        int[] a = {1, 1};
        int[] b = {3, 3};
        int[] x = {3, 2};
        int xDiff = x[0] - a[0];
        int yDiff = x[1] - a[1];
        System.out.println(Math.toDegrees(Math.atan2(yDiff, xDiff)));
        int lr = GeoUtil.links(a, b, x);
        assertTrue(lr < 0);
    }

    @Test
    public void linksRechts_Test() {
        Point a = new Point(1, 1);
        Point b = new Point(2, 1);
        Point leftOf = new Point(3, 2);
        Point rightOf = new Point(3, 0);
        Point collinear = new Point(3, 1);
        assertTrue(GeoUtil.links(a, b, leftOf).compareTo(BigInteger.ZERO) > 0);
        assertTrue(GeoUtil.links(a, b, rightOf).compareTo(BigInteger.ZERO) < 0);
        assertEquals(0, GeoUtil.links(a, b, collinear));

    }
}