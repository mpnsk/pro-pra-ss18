package de.feu.propra18.q8909806;

import de.feu.propra18.q8909806.undo.UndoManager;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

import java.math.BigInteger;
import java.util.List;
import java.util.logging.Logger;

/**
 * Wrapper um Punktklasse mit Darstellung als {@link Text}
 */
public class DisplayPoint {
    private static Logger log = Logger.getLogger(DisplayPoint.class.getName());
    private static double lastDragPosition[] = new double[2];
    Point point;
    public Circle circle;
    private double[] dragEnter;


    public DisplayPoint(Runnable renderHull, List<DisplayPoint> list, Pane pane, UndoManager undoManager) {
        circle = new Circle(5);
        circle.setOnMousePressed(event -> {
            event.consume();
            switch (event.getButton()) {
                case PRIMARY:
                    dragEnter = new double[]{event.getX(), event.getY()};
                    lastDragPosition[0] = circle.getCenterX();
                    lastDragPosition[1] = circle.getCenterY() + event.getSceneY();
                    break;
                case SECONDARY:
                    pane.getChildren().remove(circle);
                    list.remove(DisplayPoint.this);
                    renderHull.run();
                    break;
            }
        });
        circle.setOnMouseDragged(event -> {
            int deltaX = (int) event.getX();
            int deltaY = (int) (lastDragPosition[1] - event.getSceneY());
            circle.setCenterX(deltaX);
            circle.setCenterY(deltaY);
            point.setX(deltaX);
            point.setY(deltaY);
            renderHull.run();
        });
        circle.setOnMouseReleased(event -> {
                    event.consume();
                    BigInteger[] newXY = {
                            point.getX(),
                            point.getY()};
                    BigInteger[] oldXY = {
                            BigInteger.valueOf((long) (dragEnter[0])),
                            BigInteger.valueOf((long) (dragEnter[1]))};
//                    Command movePoint = new MovePoint(this, oldXY, newXY);
//                    undoManager.execute(movePoint);
                }
        );
    }

}
