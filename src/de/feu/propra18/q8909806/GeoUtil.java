package de.feu.propra18.q8909806;


import java.math.BigInteger;

public class GeoUtil {
    static int links(int[] a, int[] b, int[] c) {
        // A x (B y − C y ) + B x (C y − A y ) + C x (A y − B y )
        return a[0] * (b[1] - c[1]) +
                b[0] * (c[1] - a[1]) +
                c[0] * (a[1] - b[1]);
    }

    /**
     * Berchnet
     *
     * @param a Anfang
     * @param b Mitte
     * @param c Ende
     * @return positiv wenn c links von der Strecke a nach b, <br>
     * negativ wenn c rechts der Strecke a nach b, <br>
     * gleich 0 wenn colinear
     */
    static BigInteger links(Point a, Point b, Point c) {
        // A x (B y − C y ) + B x (C y − A y ) + C x (A y − B y )
        BigInteger axByCy = a.getX().multiply(b.getY().subtract(c.getY()));
        BigInteger bxCyAy = b.getX().multiply(c.getY().subtract(a.getY()));
        BigInteger cxAyBy = c.getX().multiply(a.getY().subtract(b.getY()));
        return axByCy.add(bxCyAy).add(cxAyBy);
    }
}
