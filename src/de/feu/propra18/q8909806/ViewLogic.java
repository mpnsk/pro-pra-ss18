package de.feu.propra18.q8909806;

import de.feu.propra18.q8909806.persistence.Persistence;
import de.feu.propra18.q8909806.undo.UndoManager;
import de.feu.propra18.q8909806.view.render.RenderingManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Nimmt Input der Benutzeroberfläche an. Verwaltet die Darstellung der Konvexen Hülle
 * MVC Controller
 */
public class ViewLogic {
    private static final Logger log = Logger.getLogger(ViewLogic.class.getName());
    public Button undo, redo;
    @FXML
    Pane pane;
    private List<DisplayPoint> displayPoints = new ArrayList<>();
    private HullCalculator hullCalculator = new HullCalculator();
    private List<Line> lines = new ArrayList<>();
    private File file;
    private Circle innerCircle;
    private UndoManager undoManager;
    private RenderingManager renderingManager;

    /**
     * JavaFX Konstruktor, wird noch vor dem Konstruktor ausgeführt.
     */
    public void initialize() {
        undoManager = new UndoManager(this::redrawGraph, undo, redo);
        renderingManager = new RenderingManager(pane, this::redrawGraph, undoManager);
    }

    /**
     * Nimmt Mausklick entgegen, lässt an der Stelle des Klicks einen Punkt eintragen und zeichnen
     *
     * @param mouseEvent
     */
    public void createPointByMouse(MouseEvent mouseEvent) {
        int x = (int) mouseEvent.getX();
        int y = (int) mouseEvent.getY();
        drawPointAt(x, y);
    }

    /**
     * Zeichnet einen Punkt in der Ebene
     *
     * @param x Abszissenwert
     * @param y Ordinatenwert
     */
    private void drawPointAt(int x, int y) {
        Point point = new Point(x, y);
        renderingManager.add(point);
        hullCalculator.getPoints().add(point);
        redrawGraph();
    }

    /**
     * Lässt den Graham Scan ausführen
     *
     * @return Konvexe Hülle
     */
    private Stack<Point> grahamScan() {
        return hullCalculator.grahamScan();
    }

    /**
     * Wandelt eine Liste von {@link DisplayPoint} zu {@link Point2D}
     *
     * @param displayPoints Liste von {@link DisplayPoint}
     * @return Liste von {@link Point2D}
     */
    private List<Point> pointFromDisplayPoints(List<DisplayPoint> displayPoints) {
        return displayPoints.stream().map(displayPoint -> displayPoint.point).collect(Collectors.toList());
    }

    /**
     * Zeichnet die konvexe Hülle als Polygon in die Ebene
     *
     * @param hull
     */
    private void drawHull(Stack<Point> hull) {
        Runnable recalculateGraph = this::redrawGraph;
        renderingManager.render(hull, hullCalculator.getInnerCircle());
    }

    public void savePoints(ActionEvent actionEvent) {
        if (file != null) save(displayPoints);
        else savePointsTo(actionEvent);
    }

    /**
     * Speichert die aktuellen Punkte in einer vom Nutzer ausgewählten Datei
     *
     * @param actionEvent
     */
    public void savePointsTo(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("../Tester/data"));
        fileChooser.setTitle("Speichern unter:");
        file = fileChooser.showSaveDialog(pane.getScene().getWindow());
        save(displayPoints);
    }

    private void save(List<DisplayPoint> displayPointList) {
        List<Point> points = pointFromDisplayPoints(displayPointList);
        List<String> pointsAsString = points.stream().map(point -> point.getX() + " " + point.getY()).collect(Collectors.toList());
        Persistence.getInstance().saveToFile(file.getAbsolutePath(), pointsAsString);
    }

    /**
     * Läd Punkte aus einer vom Nutzer ausgewählten Datei
     *
     * @param actionEvent
     */
    public void loadPoints(ActionEvent actionEvent) {
        clear(actionEvent);
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("../Tester/data"));
        fileChooser.setTitle("Datei laden:");
        FileChooser.ExtensionFilter point_file = new FileChooser.ExtensionFilter("Point file", "*.points");
        FileChooser.ExtensionFilter any_file = new FileChooser.ExtensionFilter("any file", "*");
        fileChooser.getExtensionFilters().addAll(point_file, any_file);
        String absolutePath = fileChooser.showOpenDialog(pane.getScene().getWindow()).getAbsolutePath();
        hullCalculator.addPointsFromFile(absolutePath);
        redrawGraph();
    }

    /**
     * Erstellt eine Menge von zufällig platzierten Punkten in der Ebene
     *
     * @param n Anzahl an Punkten
     */
    private void createRandomPoint(int n) {
        for (int i = 0; i < n; i++) {
            int x = (int) (Math.random() * pane.getWidth());
            int y = (int) (Math.random() * pane.getHeight());
            hullCalculator.addPoint(x, y);
            drawPointAt(x, y);
        }
    }

    public void createRandomPoints10(ActionEvent actionEvent) {
        createRandomPoint(10);
    }

    public void createRandomPoints50(ActionEvent actionEvent) {
        createRandomPoint(50);
    }

    public void createRandomPoints100(ActionEvent actionEvent) {
        createRandomPoint(100);
    }

    public void createRandomPoints500(ActionEvent actionEvent) {
        createRandomPoint(500);
    }

    public void createRandomPoints1000(ActionEvent actionEvent) {
        createRandomPoint(1000);
    }

    public void showHelp(ActionEvent actionEvent) {
        Window window = pane.getScene().getWindow();


        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(window);
        VBox dialogVbox = new VBox();
        String helpText =
                "Punkte werden hinzugefügt durch Mausklick und entfernt durch Rechtsklick. \n" +
                        "Zum verschieben die linke Maustaste gedrückt halten.";
        dialogVbox.getChildren().add(new Text(helpText));
        Scene dialogScene = new Scene(dialogVbox, 300, 200);
        dialog.setScene(dialogScene);
        dialog.show();
    }

    public void clear(ActionEvent actionEvent) {
        for (Line line : lines) pane.getChildren().remove(line);
        for (DisplayPoint p : displayPoints) pane.getChildren().remove(p.circle);
        lines.clear();
        displayPoints.clear();
        hullCalculator.clear();
        redrawGraph();
    }

    private void redrawGraph() {
        drawHull(grahamScan());
    }


    public void undo(ActionEvent actionEvent) {
        undoManager.undo();
    }

    public void redo(ActionEvent actionEvent) {
        undoManager.redo();
    }

    public void stop(ActionEvent actionEvent) {
        System.out.println("stop");
    }
}
