package de.feu.propra18.q8909806;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class HullCalculatorTest {

    @Test
    public void linksRechtsTest() {
        Point p1 = new Point(Integer.MIN_VALUE, 0);
        Point p2 = new Point(0, Integer.MIN_VALUE);
        Point p3 = new Point(Integer.MAX_VALUE, 0);
        Point p4 = new Point(0, Integer.MAX_VALUE);
        int links123 = GeoUtil.links(p1, p2, p3).intValueExact();
        assertTrue(links123 < 0);
    }

    @Test
    public void simpleSortTest(){
        Point p1 = new Point(0, 0);
        Point p2 = new Point(0, 10);
        Point p3 = new Point(10, 0);
        Point p4 = new Point(10, 10);

        List<Point> list = new ArrayList<>();
        list.addAll(Arrays.asList(p1, p2, p3, p4));
        HullCalculator hullCalculator = new HullCalculator();
        hullCalculator.sortRightToleftFromPerspective(p1, list);
        assertEquals(p1, list.get(0));
        assertEquals(p3, list.get(1));
        assertEquals(p4, list.get(2));
        assertEquals(p2, list.get(3));
    }
    @Test
    public void negativeSortTest(){
        Point p1 = new Point(0, -10);
        Point p2 = new Point(0, 10);
        Point p3 = new Point(-10, 0);
        Point p4 = new Point(10, 0);

        List<Point> list = new ArrayList<>();
        list.addAll(Arrays.asList(p1, p2, p3, p4));
        HullCalculator hullCalculator = new HullCalculator();
        hullCalculator.sortRightToleftFromPerspective(p1, list);
        assertEquals(p1, list.get(0));
        assertEquals(p4, list.get(1));
        assertEquals(p2, list.get(2));
        assertEquals(p3, list.get(3));
    }
    @Test
    public void maxNegativeSortTest(){
        Point p1 = new Point(0, Integer.MIN_VALUE);
        Point p2 = new Point(0, Integer.MAX_VALUE);
        Point p3 = new Point(Integer.MIN_VALUE, 0);
        Point p4 = new Point(Integer.MAX_VALUE, 0);

        List<Point> list = new ArrayList<>();
        list.addAll(Arrays.asList(p1, p2, p3, p4));
        HullCalculator hullCalculator = new HullCalculator();
        hullCalculator.sortRightToleftFromPerspective(p1, list);
        assertEquals(p1, list.get(0));
        assertEquals(p4, list.get(1));
        assertEquals(p2, list.get(2));
        assertEquals(p3, list.get(3));
    }

    @Test
    public void simpleHullTest() {
        HullCalculator hullCalculator = new HullCalculator();
        hullCalculator.addPoint(0, 0);
        hullCalculator.addPoint(100, 0);
        hullCalculator.addPoint(0, 100);
        hullCalculator.addPoint(100, 100);
        Stack<Point> points = hullCalculator.grahamScan();
        System.out.println("points = " + points);
    }

    @Test
    public void negativeHullTest() {
        HullCalculator hullCalculator = new HullCalculator();
        hullCalculator.addPoint(Integer.MIN_VALUE, 0);
        hullCalculator.addPoint(0, Integer.MIN_VALUE);
        hullCalculator.addPoint(Integer.MAX_VALUE, 0);
        hullCalculator.addPoint(0, Integer.MAX_VALUE);
        Stack<Point> points = hullCalculator.grahamScan();
        System.out.println("points = " + points);
    }
}
