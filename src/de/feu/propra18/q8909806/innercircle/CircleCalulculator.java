package de.feu.propra18.q8909806.innercircle;


import java.util.*;
import java.util.stream.Collectors;

public class CircleCalulculator {

    InnerCircle calculate(Point a, Point b, Point c, Point d, Point e, Point f) {
        double dAB = a.distance(b);
        double dCD = c.distance(d);
        double dEF = e.distance(f);

        double alpha1 = dAB * (d.y - c.y) - dCD * (b.y - a.y);
        double alpha2 = dCD * (f.y - e.y) - dEF * (d.y - c.y);

        double beta1 = dCD * (b.x - a.x) - dAB * (d.x - c.x);
        double beta2 = dEF * (d.x - c.x) - dCD * (f.x - e.x);

        double gamma1 = dAB * (c.x * (d.y - c.y) - c.y * (d.x - c.x))
                + dCD * (a.y * (b.x - a.x) - a.x * (b.y - a.y));
        double gamma2 = dCD * (e.x * (f.y - e.y) - e.y * (f.x - e.x))
                + dEF * (c.y * (d.x - c.x) - c.x * (d.y - c.y));

        double dH = alpha1 * beta2 - alpha2 * beta1;
        double dU = gamma1 * beta2 - gamma2 * beta1;
        double dV = alpha1 * gamma2 - alpha2 * gamma1;

        Point m = new Point();
        m.x = dU / dH;
        m.y = dV / dH;
        double r = ((a.x - m.x) * (b.y - a.y) + (a.y - m.y) * (a.x - b.x)) / dAB;

        InnerCircle innerCircle = new InnerCircle(m.x, m.y, r);
        return innerCircle;
    }

//    InnerCircle arcElimination(Point a, Point b, Point c, Point d, Point e, Point f) {
//        return new InnerCircle();
//    }

    InnerCircle arcElimination(List<Point> points) {
        if (points.size()==0) return null;
        if (points.size()==1) return new InnerCircle(points.get(0).x, points.get(0).y, 0);
        List<Arc> arcs = new ArrayList<>();
        for (int i = 0; i < points.size(); i++) {
            Arc arc = new Arc();
            arc.neighbours = arcs;
            arc.a = points.get(i);
            int nextIndex = i + 1;
            if (nextIndex == points.size()) nextIndex = 0;
            arc.b = points.get(nextIndex);
            arcs.add(arc);
        }

        while (arcs.size() > 3) {
            arcs.remove(Collections.min(arcs));
        }

        return arcs.get(0).getCircle();
    }

    public InnerCircle calculateCircle(List<de.feu.propra18.q8909806.Point> points) {
        return arcElimination(
                points.stream()
                        .map(bigIntegerPoint -> new Point(
                                bigIntegerPoint.getX().doubleValue(),
                                bigIntegerPoint.getY().doubleValue()))
                        .collect(Collectors.toList()));
    }

    class Arc implements Comparable<Arc> {
        List<Arc> neighbours;
        public Point a, b;

        @Override
        public int compareTo(Arc o) {
            return (int) (getCircle().r - o.getCircle().r);
        }

        InnerCircle getCircle() {
            int index = neighbours.indexOf(this);
            int preIndex = index == 0 ? neighbours.size() - 1 : index - 1;
            int postIndex = index == neighbours.size() - 1 ? 0 : index + 1;
            if (preIndex==postIndex) return new InnerCircle(a.x, a.y, 0);
            Arc preArc = neighbours.get(preIndex);
            Arc postArc = neighbours.get(postIndex);
            return calculate(preArc.a, preArc.b, a, b, postArc.a, postArc.b);
        }
    }
}
