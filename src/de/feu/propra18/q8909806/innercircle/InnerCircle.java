package de.feu.propra18.q8909806.innercircle;

public class InnerCircle implements Comparable<InnerCircle> {
    public double x, y, r;

    public InnerCircle(double x, double y, double r) {
        this.x = x;
        this.y = y;
        this.r = r;
    }

    @Override
    public int compareTo(InnerCircle o) {
        return (int) (r - o.r);
    }
}
