package de.feu.propra18.q8909806.innercircle;

public class Point {
    public double x, y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point() {
    }

    //(A x − B x )^2 + (A y − B y )^2
    static double distance(Point a, Point b) {
        double deltaX = a.x - b.x;
        double deltaY = a.y - b.y;
        return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
    }

    public double distance(Point other) {
        return Point.distance(this, other);
    }
}
