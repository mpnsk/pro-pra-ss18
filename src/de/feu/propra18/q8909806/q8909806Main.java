package de.feu.propra18.q8909806;

import de.feu.propra18.interfaces.IHullCalculator;
import de.feu.propra18.tester.Tester;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.IOException;

public class q8909806Main extends Application {

    public static void main(String[] args) {
        if (args.length > 0 && "-t".equals(args[0])) {
            IHullCalculator calculator = new HullCalculator();
            Tester tester = new Tester(args, calculator);
            System.out.println(tester.test());
        } else {
            launch(args);
        }
    }


    @Override
    public void start(Stage primaryStage) throws IOException {
        primaryStage.setTitle("Konvexe Hülle q8909806 Manuel Paunoski");

        Parent root = FXMLLoader.load(getClass().getResource("app.fxml"));

        Scene scene = new Scene(root, 300, 275);

        primaryStage.setScene(scene);
        primaryStage.show();

        Platform.setImplicitExit(false);
        primaryStage.setOnCloseRequest(event -> {
            showCloseDialog(primaryStage.getOwner());
            event.consume();
        });
    }

    private void showCloseDialog(Window window){
        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(window);

        Text text = new Text("Do you really want to close the application?");
        Button yes = new Button("yes");
        Button no = new Button("no");
        yes.setOnAction(event -> Platform.exit());
        no.setOnAction(event -> dialog.close());
        Pane yesno = new HBox(yes, no);
        Pane container= new VBox(text, yesno);
        Scene dialogScene = new Scene(container, 300, 200);
        dialog.setScene(dialogScene);
        dialog.show();
    }

}
