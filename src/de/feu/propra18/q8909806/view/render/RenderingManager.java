package de.feu.propra18.q8909806.view.render;

import de.feu.propra18.q8909806.Point;
import de.feu.propra18.q8909806.innercircle.InnerCircle;
import de.feu.propra18.q8909806.undo.UndoManager;
import javafx.scene.layout.Pane;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class RenderingManager {

    private static RenderingManager instance;
    private final InnerCircleRenderer innerCircleRenderer;
    private final RenderedPolygon renderedPolygon;
    private final EdgeRenderer edgeRenderer;
    private final List<RenderedPoint> renderedPoints;
    private final Runnable redrawGraph;
    private final UndoManager undoManager;
    private final Pane pane;

    public RenderingManager(Pane pane, Runnable redrawGraph, UndoManager undoManager) {
        this.innerCircleRenderer = new InnerCircleRenderer(pane);
        this.renderedPolygon = new RenderedPolygon(pane);
        this.edgeRenderer = new EdgeRenderer(pane);
        this.renderedPoints = new ArrayList<>();
        this.redrawGraph = redrawGraph;
        this.undoManager = undoManager;
        this.pane = pane;
    }

    public void render(Stack<Point> hull, InnerCircle innerCircle) {
        renderedPolygon.render(new ArrayList<>(hull));
        if (innerCircle != null) innerCircleRenderer.render(innerCircle);
    }

    public void add(Point point) {
        RenderedPoint renderedPoint = edgeRenderer.render(point);
        renderedPoints.add(renderedPoint);
        renderedPoint.makeDraggable(undoManager, redrawGraph, () -> pane.getChildren().remove(renderedPoint.getCircle()));
    }
}
