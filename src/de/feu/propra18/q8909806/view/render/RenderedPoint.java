package de.feu.propra18.q8909806.view.render;

import de.feu.propra18.q8909806.Point;
import de.feu.propra18.q8909806.undo.Command;
import de.feu.propra18.q8909806.undo.MovePoint;
import de.feu.propra18.q8909806.undo.UndoManager;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;

import java.math.BigInteger;

public class RenderedPoint {
    private final Point point;
    private final Pane pane;
    private Circle circle;


    private double lastDragPosition[] = new double[2];
    private double[] dragEnter;


    public Point getPoint() {
        return point;
    }

    public Node getCircle() {
        return circle;
    }

    public void setCircle(Circle circle) {
        this.circle = circle;
    }

    public RenderedPoint(Pane pane, Point point) {
        this.pane = pane;
        this.point = point;
    }

    public void makeDraggable(UndoManager undoManager, Runnable recalculateGrap, RemovePoint removePoint) {
        circle.setOnMousePressed(event -> {
            System.out.println("onpressed");
            event.consume();
            dragEnter = new double[]{event.getX(), event.getY()};
            switch (event.getButton()) {
                case PRIMARY:
                    lastDragPosition[0] = circle.getCenterX();
                    lastDragPosition[1] = circle.getCenterY() + event.getSceneY();
                    break;
                case SECONDARY:
                    pane.getChildren().remove(circle);
                    removePoint.run();
                    recalculateGrap.run();
                    break;
            }
        });
        circle.setOnMouseDragged(event -> {
            System.out.println("ondrag");
            int deltaX = (int) event.getX();
            int deltaY = (int) (lastDragPosition[1] - event.getSceneY());
            circle.setCenterX(deltaX);
            circle.setCenterY(deltaY);
            point.setX(deltaX);
            point.setY(deltaY);
            recalculateGrap.run();
        });
        circle.setOnMouseReleased(event -> {
            System.out.println("onrelease");
                    event.consume();
                    BigInteger[] newXY = {
                            point.getX(),
                            point.getY()};
                    BigInteger[] oldXY = {
                            BigInteger.valueOf((long) (dragEnter[0])),
                            BigInteger.valueOf((long) (dragEnter[1]))};
            Command movePoint = new MovePoint(this, oldXY, newXY);
                    undoManager.execute(movePoint);
                }
        );
    }

    public void setPointToCircle() {
        point.setX((int) circle.getCenterX());
        point.setX((int) circle.getCenterY());
    }

    public void setCircleToPoint() {
        circle.setCenterX(point.getX().doubleValue());
        circle.setCenterY(point.getY().doubleValue());
    }

    interface RemovePoint extends Runnable {
    }
}
