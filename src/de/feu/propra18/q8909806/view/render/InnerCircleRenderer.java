package de.feu.propra18.q8909806.view.render;

import de.feu.propra18.q8909806.innercircle.InnerCircle;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;

class InnerCircleRenderer {

    private final Pane pane;
    private Circle renderedCircle;

    InnerCircleRenderer(Pane pane) {
        this.pane = pane;
    }

    void render(InnerCircle innerCircle) {
        if (renderedCircle != null) pane.getChildren().remove(renderedCircle);
        renderedCircle = new Circle(innerCircle.x, innerCircle.y, innerCircle.r);
        pane.getChildren().add(renderedCircle);
    }
}
