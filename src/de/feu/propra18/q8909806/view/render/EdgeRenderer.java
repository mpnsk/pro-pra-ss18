package de.feu.propra18.q8909806.view.render;

import de.feu.propra18.q8909806.Point;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;

import java.util.ArrayList;
import java.util.List;

public class EdgeRenderer {

    private final Pane pane;
    private List<Circle> circles = new ArrayList<>();

    public EdgeRenderer(Pane pane) {
        this.pane = pane;
    }


    public RenderedPoint render(Point point) {
        Circle circle = createCircle(point);
        RenderedPoint renderedPoint = new RenderedPoint(pane, point);
        renderedPoint.setCircle(circle);
        return renderedPoint;
    }

    private Circle createCircle(Point p) {
        Circle circle = new Circle();
        double x = p.getX().doubleValue();
        circle.setCenterX(x);
        double y = p.getY().doubleValue();
        circle.setCenterY(y);
        circle.setRadius(10);
        pane.getChildren().add(circle);
        circles.add(circle);
        return circle;
    }

}
