package de.feu.propra18.q8909806.view.render;

import de.feu.propra18.q8909806.Point;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

import java.util.ArrayList;
import java.util.List;

class RenderedPolygon {

    private final Pane pane;
    private List<Line> lines = new ArrayList<>();

    RenderedPolygon(Pane pane) {
        this.pane = pane;
    }

    void render(List<Point> hull) {
        dropAllLines();

        if (!hull.isEmpty()) {
            Point first = hull.get(0);
            Point current = first;
            for (Point next : hull) {
                drawLine(current, next);
                current = next;
            }
            drawLine(current, first);
        }
    }

    private void dropAllLines() {
        for (Line l : lines) pane.getChildren().remove(l);
    }

    private void drawLine(Point start, Point end) {
        Line line = new Line(
                start.getX().intValueExact(),
                start.getY().intValueExact(),
                end.getX().intValueExact(),
                end.getY().intValueExact()
        );
        line.setFill(Color.BLUE);
        line.setStrokeWidth(1);
        line.setStroke(Color.BLUE);
        lines.add(line);
        pane.getChildren().add(line);
    }
}
