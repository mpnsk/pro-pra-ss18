package de.feu.propra18.q8909806.view.render;

public interface RecalculateCallback {
    void recalculate();
}
